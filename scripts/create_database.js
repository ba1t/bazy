
/**
 * Created by barrett on 8/28/14.
 */

var mysql = require('mysql');
var dbconfig = require('../config/database');

var connection = mysql.createConnection(dbconfig.connection);

connection.query('CREATE DATABASE ' + dbconfig.database);

connection.query('\
CREATE TABLE `' + dbconfig.database + '`.`' + dbconfig.users_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `username` VARCHAR(20) NOT NULL, \
    `password` CHAR(60) NOT NULL, \
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
)');

connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`users` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `username` VARCHAR(20) NOT NULL,\
    `password` CHAR(60) NOT NULL,\
    `email` VARCHAR(45) NULL,\
    `roles_id` INT(10) UNSIGNED NOT NULL,\
    PRIMARY KEY (`id`, `roles_id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    UNIQUE INDEX `username_UNIQUE` (`username` ASC),\
    UNIQUE INDEX `email_UNIQUE` (`email` ASC),\
    INDEX `fk_users_roles_idx` (`roles_id` ASC)\
)');
connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`hotels` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `name` VARCHAR(45) NOT NULL,\
    PRIMARY KEY (`id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    UNIQUE INDEX `hotelscol_UNIQUE` (`name` ASC)    \
)');

connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`branches` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `city` VARCHAR(45) NOT NULL,\
    `street` VARCHAR(45) NOT NULL,\
    `number` VARCHAR(45) NOT NULL,\
    `hotel_id` INT(10) UNSIGNED NOT NULL,\
    PRIMARY KEY (`id`, `hotel_id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    INDEX `fk_branches_hotels_idx` (`hotel_id` ASC),\
    CONSTRAINT `fk_branches_hotels`\
      FOREIGN KEY (`hotel_id`)\
      REFERENCES `mydb`.`hotels` (`id`)\
      ON DELETE NO ACTION\
      ON UPDATE NO ACTION)\
)');


connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`rooms` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `description` LONGTEXT NULL DEFAULT NULL,\
    `branch_id` INT(10) UNSIGNED NOT NULL,\
    PRIMARY KEY (`id`, `branch_id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    INDEX `fk_rooms_branches_idx` (`branch_id` ASC),\
    CONSTRAINT `fk_rooms_branches`\
      FOREIGN KEY (`branch_id`)\
      REFERENCES `mydb`.`branches` (`id`)\
      ON DELETE NO ACTION\
      ON UPDATE NO ACTION)\
)');

connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`images` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `url` VARCHAR(255) NULL DEFAULT NULL,\
    `room_id` INT(10) UNSIGNED NOT NULL,\
    PRIMARY KEY (`id`, `room_id`),\
    INDEX `fk_images_rooms_idx` (`room_id` ASC),\
    CONSTRAINT `fk_images_rooms`\
      FOREIGN KEY (`room_id`)\
      REFERENCES `mydb`.`rooms` (`id`)\
      ON DELETE NO ACTION\
      ON UPDATE NO ACTION)\
');

connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`roles` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `role` VARCHAR(45) NOT NULL,\
    PRIMARY KEY (`id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    UNIQUE INDEX `role_UNIQUE` (`role` ASC))\
');

connection.query('\
CREATE TABLE IF NOT EXISTS `mydb`.`reservations` (\
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,\
    `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,\
    `checkin` VARCHAR(10) NOT NULL,\
    `checkout` VARCHAR(10) NULL DEFAULT NULL,\
    `room_id` INT(10) UNSIGNED NOT NULL,\
    `user_id` INT(10) UNSIGNED NOT NULL,\
    PRIMARY KEY (`id`, `room_id`, `user_id`),\
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),\
    INDEX `fk_reservations_rooms_idx` (`room_id` ASC),\
    INDEX `fk_reservations_users_idx` (`user_id` ASC),\
    CONSTRAINT `fk_reservations_rooms`\
      FOREIGN KEY (`room_id`)\
      REFERENCES `mydb`.`rooms` (`id`)\
      ON DELETE NO ACTION\
      ON UPDATE NO ACTION,\
    CONSTRAINT `fk_reservations_users`\
      FOREIGN KEY (`user_id`)\
      REFERENCES `mydb`.`users` (`id`)\
      ON DELETE NO ACTION\
      ON UPDATE NO ACTION)\
');



console.log('Success: Database Created!')

connection.end();
