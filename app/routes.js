// app/routes.js
var mysql = require('mysql');
var dbconfig = require('../config/database');
var connection = mysql.createConnection(dbconfig.connection, {'multipleStatements': true});

connection.query('USE ' + dbconfig.database);


module.exports = function(app, passport) {
	app.get('/', function(req, res) {
			connection.query('SELECT rooms.id, rooms.branch_id, rooms.description, reservations.checkin, reservations.checkout, branches.street , branches.number, branches.city, hotels.name FROM rooms LEFT JOIN branches ON rooms.branch_id = branches.id LEFT JOIN hotels ON branches.hotel_id = hotels.id left JOIN reservations ON reservations.room_id = rooms.id GROUP BY rooms.id' , function (error, results, fields) {
				console.log(results);
				res.render('pages/index.ejs', { 
					logged: req.isAuthenticated(),
					user : req.user,
					data : results,
				});
			})
		})
	app.get('/rezerwacja/:id', function(req,res) {
		res.render('pages/reservation-form.ejs', {
			logged: req.isAuthenticated(),
			user : req.user,
			id: req.params.id,
		})
	})
	app.post('/rezerwacja/:id', function(req, res) {
		connection.query('INSERT INTO reservations SET?', [{
			"checkin": new Date(req.body.checkin), 
			"checkout":new Date(req.body.checkout), 
			"user_id": req.user.id,
			"room_id": req.params.id}],
		function (error, results, fields) {
			if (error) throw error;
			res.redirect("/")
	})
});
	app.get('/login', function(req, res) {
		res.render('pages/login.ejs', { 
			message: req.flash('loginMessage'), 
			logged: req.isAuthenticated(),
			user : req.user 
		});
	});

	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/', 
            failureRedirect : '/login', 
		}),
        function(req, res) {
        res.redirect('/');
    });
	app.get('/signup', function(req, res) {
		res.render('pages/signup.ejs', { 
			message: req.flash('signupMessage') ,
			logged: req.isAuthenticated(),
			user : req.user 
		});
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', 
		failureRedirect : '/signup', 
		failureFlash : true 
	}));

	app.get('/profile', isLoggedIn, function(req, res) {
		connection.query('SELECT rooms.id, rooms.branch_id, rooms.description, reservations.checkin, reservations.checkout, branches.street , branches.number, branches.city, hotels.name FROM rooms LEFT JOIN branches ON rooms.branch_id = branches.id LEFT JOIN hotels ON branches.hotel_id = hotels.id left JOIN reservations ON reservations.room_id = rooms.id where reservations.user_id =?', [req.user.id], function(error, results) {
			res.render('pages/profile.ejs', {
				user : req.user ,
				logged: req.isAuthenticated(),			
				results: results
			});
		})

	});

	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
	app.route('/admin/rooms/add')
		.get(isLoggedIn, adminRole, function(req,res) {
			connection.query('select * from branches', function(error, branches, fields){
				res.render('pages/rooms-form.ejs', { 
					logged: req.isAuthenticated(),
					user : req.user,
					branches: branches,
					type: 'add'
				})
			})
		})

	app.route('/admin/branches/add')
		.get(isLoggedIn, adminRole, function(req,res) {
			connection.query('select h.id, h.name from hotels h', function(error, hotels, fields){
				res.render('pages/branches-form.ejs', { 
					logged: req.isAuthenticated(),
					user : req.user,
					hotels: hotels,
					type: 'add'
				})
			})

		})
		.post(isLoggedIn, adminRole, function(req,res) {
			connection.query('INSERT INTO branches SET ?', [{"city": req.body.city, "street": req.body.street, "number": req.body.number, "hotel_id": req.body.hotel_id}], function(error, results, fields) {
				if (error) throw error;
				res.redirect("/admin/branches")
			})
		})
	app.route("/admin/users")
		.get(isLoggedIn, adminRole, function (req, res) {
			connection.query('select u.id, u.username, u.email, r.role from users u LEFT JOIN roles r on r.id = u.role_id', function(error, users){
				res.render('pages/users.ejs',{
					logged: req.isAuthenticated(),
					user : req.user,
					id: req.params.id,
					users: users,
				})
			})
		})
	app.route('/admin/users/edit/:id')
		.get(isLoggedIn, adminRole, function(req,res){
			var roles = 'select r.id, r.role from roles r';
			var user = 'select u.username, u.email, u.role_id from users u where u.id =' + req.params.id;
			connection.query(user, function(error,users) {
				connection.query(roles, function(error, roles){
					res.render('pages/users-form.ejs', {
						logged: req.isAuthenticated(),
						id: req.params.id,
						user : req.user,
						user : users[0],
						roles: roles,
						type: 'edit'
					})
				})
			})
		})
		.put(isLoggedIn, adminRole, function(req, res) {
			connection.query('UPDATE users SET ? where id=?', [{"username": req.body.username, "email": req.body.email, "role_id": req.body.role_id }, req.params.id], function(error, results) {
				
				if (error) throw error;
				res.redirect("/admin/users")
			})
		})
	
	app.route('/admin/branches/edit/:id')
		.get(isLoggedIn, adminRole, function(req,res) {
			var hotels = 'select h.id, h.name from hotels h'
			var branch = 'select * from branches where id=' + req.params.id;
			var rooms = 'select * from rooms where branch_id=' + req.params.id;
			connection.query(branch, function(error, branch){
				connection.query(hotels, function(error, hotels){
					connection.query(rooms, function(error, rooms){
						res.render('pages/branches-form.ejs', { 
							logged: req.isAuthenticated(),
							user : req.user,
							branch : branch[0],
							hotels: hotels,
							rooms: rooms,
							type: 'edit'
						})
					})
				})
			})
		})
		.put(isLoggedIn, adminRole, function(req,res) {
			connection.query('UPDATE branches SET ? where id=?', [{"city": req.body.city, "street": req.body.street, "number": req.body.number, "hotel_id": req.body.hotel_id},req.params.id], function(error, results, fields) {
				if (error) throw error;
				res.redirect("/admin/branches")
			})
		})
	app.route('/admin/branches/edit/:id/rooms')
		.get(isLoggedIn, adminRole, function(req,res) {
			connection.query('select * from branches', function(error, branches, fields){
				res.render('pages/rooms-form.ejs', { 
					logged: req.isAuthenticated(),
					user : req.user,
					id: req.params.id,
					branches: branches,
					type: 'add'
				})
			})
		})
		.post(isLoggedIn, adminRole, function(req,res) {
			connection.query('INSERT INTO rooms SET ?', [{"price": req.body.price, "people": req.body.people, "description": req.body.description, "branch_id": req.params.id}], function(error, results, fields) {
				if (error) throw error;
				res.redirect("/admin/branches/edit/" + req.params.id)
			})
		})
	app.get('/admin/branches', isLoggedIn, adminRole, function(req,res) {
		connection.query('SELECT b.id, b.street, b.number, b.city, h.name as hotel_name from branches b LEFT JOIN hotels h on h.id = b.hotel_id ORDER by h.name', function(error, results, fields){
			res.render('pages/branches.ejs', { 
				logged: req.isAuthenticated(),
				user : req.user,
				data: results
			})
		})

	})

	app.get('/admin/rezerwacje', isLoggedIn,adminRole, function(req, res){
        connection.query('SELECT reservations.id, DATE_FORMAT( reservations.created , "%d-%m-%Y %T") as created, reservations.checkin, reservations.checkout, users.username, users.email, branches.street , branches.number, branches.city FROM `reservations` LEFT JOIN users ON reservations.user_id = users.id LEFT JOIN rooms ON reservations.room_id = rooms.id LEFT JOIN branches ON rooms.branch_id = branches.id group by reservations.id', function (error, results, fields) {
			res.render('pages/reservation.ejs', { 
				logged: req.isAuthenticated(),
				user : req.user,
				data: results
			})
        });
	})
	app.get('/admin/rezerwacje/:id',isLoggedIn,adminRole,  function(req, res){
        connection.query('SELECT * FROM reservations WHERE id=?',req.params.id, function (error, results, fields) {
			console.log(results)
            res.send(results);
        });
	})


};

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

function adminRole(req, res, next) {
	if(req.user.role_id == 1) 
		return next();
	res.redirect('/')
}
